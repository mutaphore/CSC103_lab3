public class LQueue <T>
{
   private Node front;
   private Node end;
   
   private class Node
   {
      public T value;
      public Node next;
   }
   
   public static class MyException extends RuntimeException
   {
      public MyException()
      {
         super();
      }
      public MyException(String message)
      {
         super(message);
      }
   }
   
   public LQueue()
   {
      front = null;
      end = null;
   }
   
   public void enqueue (T data)
   {
      Node newNode = new Node();
      newNode.value = data;
      
      if(front != null)
      {         
         end.next = newNode;
         end = newNode;
      }
      else
      {
         front = newNode;
         end = newNode;
      }
   }
   
   public T dequeue () 
   {
      T returnVal;
      
      if(front != null)
      {
         returnVal = front.value;
         front = front.next;
      }
      else
      {
         throw new MyException();
      }
      return returnVal;
   }
   
   public boolean isEmpty()
   {
      return front == null;
   }
   
}
