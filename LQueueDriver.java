import java.util.*;

public class LQueueDriver {

   public static void main(String[] args)
   {
      LQueue<Integer> queue = new LQueue<Integer>();
      Scanner input = new Scanner(System.in);
      char choice = 'z';
      int enqueue_count = 0;
      int dequeue_count = 0;
      int temp;
      
      System.out.println("Choose one of the following operations:");
      System.out.println("- enqueue/add (enter the letter a)");
      System.out.println("- dequeue/delete (enter the letter d)");
      System.out.println("- check if the list is empty (enter the letter e)");
      System.out.println("- quit (enter the letter q)");
     
      while(choice != 'q')
      {
         System.out.println("Enter choice: ");
         choice = input.nextLine().charAt(0);

         switch (choice)
         {
            case 'a':
               System.out.println("Please enter value to be added to queue: ");
               if(input.hasNextInt())
               {
                  temp = input.nextInt();
                  input.nextLine();
                  queue.enqueue(temp);
                  enqueue_count++;
                  System.out.println("Added " + temp);
                  System.out.println(enqueue_count + " enqueued");  //Not required
               }
               else
               {
                  System.out.println("Invalid value!");
                  input.nextLine();
               }
               break;
            case 'd':
               try
               {
                  temp = queue.dequeue();
                  dequeue_count++;
                  System.out.println("Removed " + temp);
                  System.out.println(dequeue_count + " dequeued"); //Not required
               }
               catch (LQueue.MyException e)
               {
                  System.out.println("Invalid operation: queue is empty!");
               }
               break;
            case 'e':
               if(queue.isEmpty())
                  System.out.println("empty");
               else
                  System.out.println("not empty");
               break;
            case 'q':
                  System.out.println("Quitting");
               break;
            default:
               System.out.println("Invalid choice!");
               break;     
         }
      }
            
      while(!queue.isEmpty())
      {
         System.out.print(queue.dequeue() + " ");
      }
   }
   

}
